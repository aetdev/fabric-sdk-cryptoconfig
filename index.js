let path = require('path');
const fs = require('fs-extra');
const util = require('util');


const CryptoConfig = class {
    constructor(ordererOrgs, peerOrgs) {
        this.OrdererOrgs = [];
        this.PeerOrgs = [];
    }

    addOrdererOrg (name_or_ordererOrg, domain, specs, template, users, enableNodeOUs, ca) {
        if (name_or_ordererOrg instanceof Org) {
            this.OrdererOrgs.push(name_or_ordererOrg);
        } else {
            let ordererOrg = new Org(name_or_ordererOrg, domain, specs, template, users, enableNodeOUs, ca);
            this.OrdererOrgs.push(ordererOrg);
        }
    }

    addPeerOrg (name_or_PeerOrg, domain, specs, template, users, enableNodeOUs, ca) {
        if (name_or_PeerOrg instanceof Org) {
            this.PeerOrgs.push(name_or_PeerOrg);
        } else {
            let PeerOrg = new Org(name_or_PeerOrg, domain, specs, template, users, enableNodeOUs, ca);
            this.PeerOrgs.push(PeerOrg);
        }
    }

    toJson(format = false) {
        let res = {
            OrdererOrgs: this.OrdererOrgs,
            PeerOrgs: this.PeerOrgs
        };

        let json = "";

        if (format) {
            json = JSON.stringify(res, null, 2);
        } else {
            json = JSON.stringify(res);
        }

        return json;
    }

    saveToFile(configPath, filename = "crypto-config.json") {
        configPath = (configPath) ? configPath : this.constructor.configFileLocation;

        if (!fs.pathExistsSync(configPath)){
            fs.ensureDirSync(configPath);
        }

        fs.writeFileSync(configPath+filename, this.toJson(true));
    }


    static get configFileLocation() {
        return path.join(RootPATH , '../network/config/network/');
    }
};

const Org = class {

    constructor(name, domain, specs, template, users, enableNodeOUs, ca) {

        if (!name) {
            throw new Error('Missing name parameter');
        }
        if (!domain) {
            throw new Error('Missing domain parameter');
        }

        this.Name = name;
        this.Domain = domain;

        if (specs) {
            if (Array.isArray(specs)) {
                this.Specs = specs;
            } else if (specs instanceof Spec) {
                this.Specs = [specs];
            } else {
                throw new Error('Missing specs parameter');
            }

        }

        if (enableNodeOUs) {
            this.EnableNodeOUs = enableNodeOUs;
        }

        if (typeof template === 'number' || typeof template === 'string') {
            this.Template = new Template(parseInt(template));
        } else if (typeof template === 'object' || template instanceof Template) {
            this.Template = template
        }

        if (typeof users === 'number' || typeof template === 'string') {
            this.Users = {Count: parseInt(users)}
        } else if (typeof users === 'object') {
            this.Users = users
        }

        if (ca) {
            this.CA = ca;
        }
    }

    addSpec(hostName_or_Spec, commonName, sans, country, province, locality, organizationalUnit, streetAddress, postalCode) {
        if (hostName_or_Spec instanceof Spec) {
            this.Specs.push(hostName_or_Spec);
        } else {
            let spec = new Spec(hostName_or_Spec, commonName, sans, country, province, locality, organizationalUnit, streetAddress, postalCode);
            this.Specs.push(spec);
        }
    }
};

const Spec = class {
    constructor(hostName, commonName, sans, country, province, locality, organizationalUnit, streetAddress, postalCode) {
        if (hostName) {
            this.Hostname = hostName;
        }

        if (commonName) {
            this.CommonName = commonName;
        }

        if (country) {
            this.Country = country;
        }

        if (province) {
            this.Province = province;
        }

        if (locality) {
            this.Locality = locality;
        }

        if (organizationalUnit) {
            this.OrganizationalUnit = organizationalUnit;
        }

        if (streetAddress) {
            this.StreetAddress = streetAddress;
        }

        if (postalCode) {
            this.PostalCode = postalCode;
        }

        if (sans) {
            this.SANS = [];
            if (typeof sans === 'string') {
                this.SANS.push(sans);
            } else if (Array.isArray(sans)) {
                this.SANS = sans;
            }
        }

    }
};

const Users = class {
    constructor(count) {
        this.Count = count;
    }
};

const Template = class {
    constructor(count, start, hostname, sans) {
        if (count) {
            this.Count = count;
        }

        if (start) {
            this.Start = start;
        }

        if (hostname) {
            this.Hostname = hostname;
        }

        if (sans) {
            this.SANS = sans;
        }
    }
};

module.exports = CryptoConfig;
module.exports.Org = Org;
module.exports.Spec = Spec;
module.exports.Users = Users;
module.exports.Template = Template;