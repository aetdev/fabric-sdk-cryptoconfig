declare class CryptoConfig {
    OrdererOrgs: CryptoConfig.Org[];
    PeerOrgs: CryptoConfig.Org[];

    constructor(
        ordererOrgs: CryptoConfig.Org[],
        peerOrgs: CryptoConfig.Org[]
    );

    /**
     * Definition of organizations managing orderer nodes
     * @param name
     * @param domain
     * @param specs
     * @param template
     * @param users
     * @param enableNodeOUs
     * @param ca
     */
    addOrdererOrg (
        name: CryptoConfig.Org | string,
        domain?: string,
        specs?: CryptoConfig.Spec[]|CryptoConfig.Spec,
        template?: CryptoConfig.Template|number,
        users?: CryptoConfig.Users|number,
        enableNodeOUs?: boolean,
        ca?: CryptoConfig.Spec
    );

    /**
     * Definition of organizations managing peer nodes
     * @param name
     * @param domain
     * @param specs
     * @param template
     * @param users
     * @param enableNodeOUs
     * @param ca
     */
    addPeerOrg (
        name: CryptoConfig.Org | string,
        domain?: string,
        specs?: CryptoConfig.Spec[]|CryptoConfig.Spec,
        template?: CryptoConfig.Template|number,
        users?: CryptoConfig.Users|number,
        enableNodeOUs?: boolean,
        ca?: CryptoConfig.Spec
    );

    public toJson(format?: boolean): string;

    public saveToFile(configPath?: string, filename?: string): void;

    public static configFileLocation(): string;
}
export = CryptoConfig;


declare namespace CryptoConfig {
    export class Org {
        Name: string;
        Domain: string;
        EnableNodeOUs: boolean;
        CA: Spec;
        Template: Template;
        Specs: Spec[];
        Users: Users;

        constructor(name: string, domain: string, specs?: Spec|Spec[], enableNodeOUs?: boolean, template?: Template, users?: Users, ca?: Spec);

        addSpec(hostName_or_Spec: Spec | string, commonName: string, sans: string[], country?: string, province?: string, locality?: string, organizationalUnit?: string, streetAddress?: string, postalCode?: string);
    }


    export class Spec {
        Hostname: string;
        CommonName?: string;
        SANS?: string[];
        Country?: string;
        Province?: string;
        Locality?: string;
        OrganizationalUnit?: string;
        StreetAddress?: string;
        PostalCode?: string;

        constructor(hostName?: string, commonName?: string, sans?: string[], country?: string, province?: string, locality?: string, organizationalUnit?: string, streetAddress?: string, postalCode?: string)
    }
    
    export class Users {
        Count: number;

        constructor(count: number);
    }
    
    export class Template {
        Count: number;
        Start: number;
        Hostname: string;
        SANS: string[];

        constructor(count?: number, start?: number, hostname?: string, sans?: string[])
    }
}